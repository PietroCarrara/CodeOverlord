using System;

namespace Overlord
{
	public class VirtualFile
	{
		public string Text;

		public VirtualFile(string s)
		{
			this.Text = s;
		}
	}
}
